var express = require('express');
var path = require('path');
var mongodb = require('mongodb');
var bodyParser = require('body-parser');
var monk = require('monk');
var db = monk('mongodb://toitus:kekeke17@ds051960.mongolab.com:51960/ciswebdevdb');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

/*app.use(function(req, res, next) {
	req.db = db;
	next();
});*/

//Routes
/**********************************************************************/

app.get('/home', function(req, res) {
	res.render('landing.html');
});

app.get('/register', function(req, res) {
	res.render('register.html');
});

app.post('/checkuser', function(req, res) {
	var user = req.body.user.toLowerCase();
	var collection = db.get("users");

	var source = "";

	res.writeHead(200, "Content-Type" - "text/plain");

	collection.findOne({"username": user}, function(err, doc) {
		if (doc === null && user != "") {
			source = "y";
			console.log(source);
			res.end(source);
		} else {
			source = "n";
			console.log(source);
			res.end(source);			
		}
	});	
});

app.post('/registeruser', function(req, res) {
	var user = req.body.user.toLowerCase();
	var pass = req.body.pass;
	var collection = db.get("users");
	
	collection.insert({"username" : user, "password" : pass}, function() {
		res.end();
	});

});

app.get('/login', function(req, res) {
	res.render('login.html');
});

/**********************************************************************/

app.listen(3000, function() {
	console.log('Listening on port 3000...');
});

module.exports = app;
